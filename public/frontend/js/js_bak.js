function getName(name){
	alert(name);
}
/*Tạo component */
//<Album>
var Album = React.createClass({
	next(){
		this.setState({img: this.state.img == 7?7:this.state.img +1});
	},
	prev(){
		this.setState({img: this.state.img == 1?1:this.state.img -1});
	},
	getInitialState:function (){
		return{
			img : 1
		};
	},
	componentDidMount(){
		setInterval(()=>{console.log("123")},1000);
	},
	render:function(){
		return(
			<div>
				<img src="frontend/img/smiley.gif" />
				<button onClick={this.next}>Tiếp theo</button>
				<button onClick={this.prev}>Quay lại</button>
			</div>
		);
	}
});
///inputTagcomponent
var InputTag = React.createClass({
	handleChange:function (event) {
		alert(event.target.value);
		this.setState({value: event.target.value});
	},
	show:function(event){
		var text = this.refs.txt.value;
		var text_sl = this.refs.sl.value;
		alert(text_sl);
	},
	getInitialState(){
		return ({
			value : ""
		});
	},
	render : function(){
		return(
			<div>
				<select ref="sl">
					<option value="a">AAA</option>
					<option value="b">BBB</option>
					<option value="c">CCC</option>
				</select>
				<input type="text" ref="txt" onChange={this.handleChange} />
				<button onClick={this.show}>Hien thi</button>
				<span>{this.state.value}</span>
			</div>
		)
	}
});
/////////////////////////////////////CartComponent
var CartCpn = React.createClass({
	handleChange:function (event) {
		this.setState({value: event.target.value});
	},
	addStudent:function () {
		this.state.tongHocVien = parseInt(this.state.tongHocVien) +1 ;
		this.state.tongHocVienall = parseInt(this.state.tongHocVienall)+2 ;
		this.setState(this.state);
	},
	getName_bak:function(){
		getName(this.props.children);
	},
	layThongtin:function(){
		alert(this.props.children);
	},
	getInitialState(){
		return {
			tongHocVien : this.props.tongHocVien,
			tenhocvien : this.props.tenhocvien,
			tongHocVienall : this.props.tongHocVienall,
			value : ""
		};
	},
	render: function () {
		return(
			<div className="container">
				<h3 className="title">Items in your cart</h3>
				{/*Cách bắt sự kiện onChange*/}
				<form>
				  <label>
				    Name:
				    <input type="text" name="name" value={this.state.value} onChange={this.handleChange}/>
				  </label>
				  <input type="submit" value="Submit" />
				</form>
				<div>{this.state.value}</div>
				{/*Hết bắt sự kiện onChange*/}
				<div>Số học viên: {this.state.tongHocVien}</div>
				<div>Số học viên all: {this.state.tongHocVienall}</div>
				<div>Tên học viên: {this.state.tenhocvien}</div>
				<button onClick={this.addStudent}>Thêm học viên</button>
				<CartListCpn/>
				<ButtonCpn onClick={this.layThongtin}>Update cart</ButtonCpn>
				<ButtonCpn onClick={
					()=>{
						var str = this.props.ten +" " + this.props.monan;getName(str);
					}
				}>Update cart mới sử dụng arrow function</ButtonCpn>

				{/*---------------------------------------- Cách 1 ---------------------------------------*/}
				<button onClick={this.getName_bak}>thongtin</button>

				{/*-----------------------------------------Cách 2 ---------------------------------------*/}
				<button onClick={()=>{getName(this.props.children)}}>laythongtinnhanh</button>

				<button onClick={()=>{this.getName_bak(this.props.children)}}>laythongtinnhanh2</button>

				<ButtonCpn>Continue shopping</ButtonCpn>
				<div className="checkoutbt">
					<ButtonCpn>Checkout</ButtonCpn>
				</div>
			</div>
		);
	}
});
/////////////////////////////////////CartListCpn
var CartListCpn = React.createClass({
	render: function () {
		return(
			<div className="table">
				<table className="table-div">
					<thead>
					<tr>
						<th>Product</th>
						<th></th>
						<th>Qty</th>
						<th>Total</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
						<CartItemCpn img="frontend/img/smiley.gif" nameproduct="ajnomoto" costproduct="200.00vnd" />
					</tbody>
				</table>
			</div>
		);
	}
});
////////////////////////////////////////CartItemCpn
var CartItemCpn = React.createClass({
	render: function () {
		return(
			<tr>
				<td><img src={this.props.img} alt="Smiley face" height="42" width="42"/></td>
				<td>
				<p>{this.props.nameproduct}</p>
				<p>{this.props.costproduct}</p>
				</td>
				<td>
					<select>
						<option>1</option>
						<option>2</option>
					</select>
				</td>
				<td>ThanhGia</td>
				<td><ButtonCpn>Remove</ButtonCpn></td>
			</tr>
		);
	}
});
////////////////////////////////////////ButtonComponent
var ButtonCpn = React.createClass({
	render: function () {
		return(
			<div onClick={this.props.onClick}>
				<button type="button">{this.props.children}</button>
			</div>
		);
	}
});


ReactDOM.render(
	<div>
		<Album></Album>
		<InputTag></InputTag>
		<CartCpn cp="ReactJS" demo="hihi" ten="Phở" monan="Gà" tongHocVien="20" tenhocvien="huy" tongHocVienall="20">Không hành</CartCpn>
	</div>
, document.getElementById("root"));
