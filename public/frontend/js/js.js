var list;

class Note extends React.Component{
	render() {
		return (
			<div className="div-note">{this.props.children}</div>
		);
	}
}

class IndputDiv extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.send = this.send.bind(this);
	}

	send(){
		list.setState({mang: this.refs.txt.value.concat(list.state.mang)});
		ReactDOM.unmountComponentAtNode(document.getElementById('div-add'));
	}

	render() {
		return(
			<div className="">
				<input type="text" ref="txt" placeholder="Enter yourname" />
				<button onClick={this.send}>Gửi</button>
			</div>
		);
	}
}

// add2 = ()=>{console.log("1234");}

class List extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.addDiv = this.addDiv.bind(this);
		//thủ thuật cách đưa một component vào một action của một component khác để cp đó tác động đến cp này
		list = this;
		this.state ={mang:[]};
	}

	addDiv = ()=>{
		ReactDOM.render(<IndputDiv/>, document.getElementById("div-add"));
	}

	render() {
		return(
			<div className="div-list">
				<div id="div-add"></div>
				<button onClick={this.addDiv} >Thêm</button>
				{
					this.state.mang.map(function(note,index){
						return <Note key={index}>{note}</Note>
					})
				}
			</div>
		);
	}

	componentDidMount(){
		var that = this;
		$.post("/getNotes",function(data){
			that.setState({mang:data});
		});
	}
}

ReactDOM.render(
	<div>
		<List />
	</div>
, document.getElementById("root"));
