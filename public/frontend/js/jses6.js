class Note extends React.Component{
	render() {
		return (
			<p>{this.props.children}</p>
		);
	}
}

class List extends React.Component{
	constructor(props, context){
		super(props, context);
		this.add = this.add.bind(this);
		this.state ={mang:[
			{srcHinh:"1",noiDung:"Hello"},
			{srcHinh:"2",noiDung:"Hi"},
			{srcHinh:"3",noiDung:"KhoaPham"}
		]};
	}

	handleChange = (e) => e.preventDefault()

	add = (e) => {e.preventDefault();
								this.state.mang.unshift({srcHinh:"Node js",noiDung:"ReactJS"});
								this.setState(this.state);}


	render(){
		return(
			<div>
			<a href="google.vn" onClick={this.add}>Add</a>
			{
					this.state.mang.map(function(note, index){
						return <Note key={index}>{note.srcHinh}+{note.noiDung}</Note>
					})
			}
			</div>
		);
	}
}

function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

const element = <Welcome name="Sara" />;

ReactDOM.render(
	<div>
		<List></List>
	</div>
, document.getElementById("root"));
